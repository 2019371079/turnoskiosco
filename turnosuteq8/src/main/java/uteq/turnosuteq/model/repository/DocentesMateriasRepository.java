/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/springframework/Repository.java to edit this template
 */
package uteq.turnosuteq.model.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uteq.turnosuteq.model.entity.DocentesMateriasModel;

/**
 *
 * @author Jesus
 */
public interface DocentesMateriasRepository extends JpaRepository<DocentesMateriasModel, Long> {
    
}
