/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/springframework/RestController.java to edit this template
 */
package uteq.turnosuteq.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import uteq.turnosuteq.model.entity.MateriasModel;
import uteq.turnosuteq.model.repository.MateriasRepository;

/**
 *
 * @author Jesus
 */
@RestController
@RequestMapping("/materiasRest")
public class MateriasRestController {
    
        
   @Autowired
    private MateriasRepository materiasRepository;
    
    @CrossOrigin(origins = "*")
    @GetMapping()
    public List<MateriasModel> list() {
        return materiasRepository.findAll();
    }
    
    @CrossOrigin(origins = "*")
    @GetMapping("/{id}")
    public MateriasModel get(@PathVariable Long id) {
        return materiasRepository.findById(id).orElse(null);
    }
    
    @CrossOrigin(origins = "*")
    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable Long id, @RequestBody MateriasModel input) {
        
        MateriasModel materiaActual = materiasRepository.findById(id).orElse(null);

        if (materiaActual == null) {
            return new ResponseEntity<>(materiaActual, HttpStatus.NOT_FOUND);
        }

        materiaActual.setNombre(input.getNombre());

        MateriasModel materiaActualizada = materiasRepository.save(materiaActual);
        HttpStatus generatedStatus = HttpStatus.CREATED;

        return new ResponseEntity<>(materiaActualizada, generatedStatus);
    }
    
    @CrossOrigin(origins = "*")
    @PostMapping
    public ResponseEntity<?> post(@RequestBody MateriasModel input) {
        return new ResponseEntity<>(materiasRepository.save(input), HttpStatus.CREATED);
    }
    
    @CrossOrigin(origins = "*")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        materiasRepository.deleteById(id);
        return new ResponseEntity<>("Borrado", HttpStatus.OK);
    }
    
}
