/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/springframework/RestController.java to edit this template
 */
package uteq.turnosuteq.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import uteq.turnosuteq.model.entity.TurnosModel;
import uteq.turnosuteq.model.repository.TurnosRepository;

/**
 *
 * @author Jesus
 */
@RestController
@RequestMapping("/turnosRest")
public class TurnosRestController {
    
     @Autowired
    private TurnosRepository turnosRepository;
    
    @CrossOrigin(origins = "*")
    @GetMapping()
    public List<TurnosModel> list() {
        return turnosRepository.findAll();
    }
    
    @CrossOrigin(origins = "*")
    @GetMapping("/{id}")
    public TurnosModel get(@PathVariable Long id) {
        return turnosRepository.findById(id).orElse(null);
    }
    
    @CrossOrigin(origins = "*")
    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable Long id, @RequestBody TurnosModel input) {
        
        TurnosModel turnoActual = turnosRepository.findById(id).orElse(null);

        if (turnoActual == null) {
            return new ResponseEntity<>(turnoActual, HttpStatus.NOT_FOUND);
        }

        turnoActual.setMatricula(input.getMatricula());
        turnoActual.setEstado(input.getEstado());
        turnoActual.setFecha(input.getFecha());
        turnoActual.setId_usuario(input.getId_usuario());
        turnoActual.setId_materia(input.getId_materia());

        TurnosModel turnoActualizado = turnosRepository.save(turnoActual);
        HttpStatus generatedStatus = HttpStatus.CREATED;

        return new ResponseEntity<>(turnoActualizado, generatedStatus);
    }
    
    @CrossOrigin(origins = "*")
    @PostMapping
    public ResponseEntity<?> post(@RequestBody TurnosModel input) {
        return new ResponseEntity<>(turnosRepository.save(input), HttpStatus.CREATED);
    }
    
    @CrossOrigin(origins = "*")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        turnosRepository.deleteById(id);
        return new ResponseEntity<>("Borrado", HttpStatus.OK);
    }
    
}
