/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/springframework/RestController.java to edit this template
 */
package uteq.turnosuteq.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import uteq.turnosuteq.model.entity.UsuariosModel;
import uteq.turnosuteq.model.repository.UsuariosRepository;

/**
 *
 * @author Jesus
 */
@RestController
@RequestMapping("/usuariosRest")
public class UsuariosRestController {
    
    @Autowired
    private UsuariosRepository usuariosRepository;
    
    @CrossOrigin(origins = "*")
    @GetMapping()
    public List<UsuariosModel> list() {
        return usuariosRepository.findAll();
    }
    
    @CrossOrigin(origins = "*")
    @GetMapping("/{id}")
    public UsuariosModel get(@PathVariable Long id) {
        return usuariosRepository.findById(id).orElse(null);
    }
    
    @CrossOrigin(origins = "*")
    @PutMapping("/{id}")
    public ResponseEntity<?> put(@PathVariable Long id, @RequestBody UsuariosModel input) {
        
        UsuariosModel usuarioActual = usuariosRepository.findById(id).orElse(null);

        if (usuarioActual == null) {
            return new ResponseEntity<>(usuarioActual, HttpStatus.NOT_FOUND);
        }

        usuarioActual.setCorreo(input.getCorreo());
        usuarioActual.setPassword(input.getPassword());
        usuarioActual.setTipo(input.getTipo());
        usuarioActual.setNombre(input.getNombre());
        usuarioActual.setApellido(input.getApellido());
        usuarioActual.setId_cubiculo(input.getId_cubiculo());

        UsuariosModel usuarioActualizado = usuariosRepository.save(usuarioActual);
        HttpStatus generatedStatus = HttpStatus.CREATED;

        return new ResponseEntity<>(usuarioActualizado, generatedStatus);
    }
    
    @CrossOrigin(origins = "*")
    @PostMapping
    public ResponseEntity<?> post(@RequestBody UsuariosModel input) {
        return new ResponseEntity<>(usuariosRepository.save(input), HttpStatus.CREATED);
    }
    
    @CrossOrigin(origins = "*")
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        usuariosRepository.deleteById(id);
        return new ResponseEntity<>("Borrado", HttpStatus.OK);
    }
    
}
